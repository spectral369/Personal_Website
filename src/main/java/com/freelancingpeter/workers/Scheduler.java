package com.freelancingpeter.workers;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.freelancingpeter.data.AccountsInfo;

@Configuration
@EnableScheduling
@EnableAsync
public class Scheduler {

	@Async
	@Scheduled(cron = "0 0 */3 * * *")
	public void fetchGamesInfo() {
		new SteamLoading(AccountsInfo.getInstance().getSTEAMKEY(), AccountsInfo.getInstance().getSTEAMID());
		new MastodonLoading(AccountsInfo.getInstance().getMASTODONTOKEN(), AccountsInfo.getInstance().getMASTODONID());
		new LOLLoading(AccountsInfo.getInstance().getLOLAPIKEY(), AccountsInfo.getInstance().getLOLID());
	}

	@EventListener(ApplicationReadyEvent.class)
	public void atStartup() {
		new SteamLoading(AccountsInfo.getInstance().getSTEAMKEY(), AccountsInfo.getInstance().getSTEAMID());
		new MastodonLoading(AccountsInfo.getInstance().getMASTODONTOKEN(), AccountsInfo.getInstance().getMASTODONID());
		new LOLLoading(AccountsInfo.getInstance().getLOLAPIKEY(), AccountsInfo.getInstance().getLOLID());
	}

}
