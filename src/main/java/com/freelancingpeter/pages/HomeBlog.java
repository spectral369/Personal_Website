package com.freelancingpeter.pages;

import java.util.ArrayList;
import java.util.List;

import com.freelancingpeter.data.Post;
import com.freelancingpeter.data.PostsRepository;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;

public class HomeBlog extends VerticalLayout {

	private static final long serialVersionUID = -5436484645683011941L;
	private int currentLastId = 0;
	private int buttonSelected = 1;
	private int postsPerCurrentPage = 0;

	private VerticalLayout contentParent = null;
	private TextField search = null;
	private VerticalLayout postContent = null;
	private Scroller scroller = null;
	private HorizontalLayout footer = null;
	private Button pageBtn = null;
	private String currentSearchVal;
	List<Post> searchResults = null;
	private int numOfPages;

	private List<Integer> test = null;

	public HomeBlog(PostsRepository postRepo) {
		setMargin(false);
		setPadding(false);
		setSpacing(false);
		contentParent = new VerticalLayout();
		contentParent.setMargin(false);
		contentParent.setPadding(false);
		contentParent.setSizeFull();
		contentParent.setAlignItems(Alignment.CENTER);

		test = new ArrayList<Integer>();

		search = new TextField();
		search.setWidth("75%");
		search.setPlaceholder("Search...");
		search.setValueChangeMode(ValueChangeMode.TIMEOUT);
		search.setValueChangeTimeout(1000);
		search.addInputListener(event -> {
			if (event.getSource().equals(search)) {
				currentSearchVal = search.getValue().trim();
			}
			searchResults = postRepo.getSearchResults(currentSearchVal);

			test.clear();
			for (Post res : searchResults) {
				test.add(res.getId());
			}
			if (searchResults.size() > 0) {

				numOfPages = Double.valueOf(Math.ceil(Double.valueOf(searchResults.size()) / 4)).intValue();
			} else {

				numOfPages = Double.valueOf(Math.ceil(Double.valueOf(postRepo.count()) / 4)).intValue();
			}

			postContent.removeAll();
			Integer[] arr = test.toArray(new Integer[test.size()]);

			for (Post p : postRepo.getFirstPageItems(!test.isEmpty(), arr)) { // new int[] {6,7}
				PostBox post1 = new PostBox(p.getTitle(), p.getSummary(), p.getBody(), p.getAuthor(), p.getDate());
				postContent.add(post1);
				currentLastId = p.getId();

			}

			setNextpages(postRepo);
		});

		contentParent.add(search);

		postContent = new VerticalLayout();
		postContent.setSizeFull();
		postContent.setAlignItems(Alignment.CENTER);

		numOfPages = Double.valueOf(Math.ceil(Double.valueOf(postRepo.count()) / 4)).intValue();

		if (numOfPages < 1) {
			H3 noPosts = new H3("No Posts");
			postContent.add(noPosts);
		} else {

			for (Post p : postRepo.getFirstPageItems(!test.isEmpty(), test.toArray(new Integer[test.size()]))) {
				PostBox post1 = new PostBox(p.getTitle(), p.getSummary(), p.getBody(), p.getAuthor(), p.getDate());
				postContent.add(post1);
				currentLastId = p.getId();
				buttonSelected = 1;
			}
		}

		scroller = new Scroller(postContent);
		scroller.setScrollDirection(Scroller.ScrollDirection.VERTICAL);
		scroller.setSizeFull();
		contentParent.add(scroller);
		// footer
		footer = new HorizontalLayout();
		footer.setWidthFull();

		pageBtn = new Button(String.valueOf(1));

		pageBtn.addThemeVariants(ButtonVariant.LUMO_ICON);
		pageBtn.setAriaLabel("Next Page");
		pageBtn.setId(String.valueOf(1));
		pageBtn.setTooltipText("Go to page " + 1);
		pageBtn.addClickListener(event -> {
			postContent.removeAll();
			for (Post p : postRepo.getFirstPageItems(!test.isEmpty(), test.toArray(new Integer[test.size()]))) {
				PostBox post1 = new PostBox(p.getTitle(), p.getSummary(), p.getBody(), p.getAuthor(), p.getDate());
				postContent.add(post1);
				currentLastId = p.getId();
				buttonSelected = 1;

			}
			pageBtn.getParent().get().getChildren().forEach(item -> {
				if (item instanceof Button bt) {
					bt.setEnabled(true);
				}
			});
		});
		footer.add(pageBtn);

		setNextpages(postRepo);

		footer.setAlignItems(Alignment.CENTER);
		footer.setJustifyContentMode(JustifyContentMode.CENTER);

		add(contentParent);
		add(footer);
		setAlignSelf(Alignment.END, footer);
		setSizeFull();

	}

	private void setNextpages(PostsRepository postRepo) {
		footer.removeAll();
		footer.add(pageBtn);

		for (int i = 2; i <= numOfPages; i++) {
			Button pageBtnNext = new Button(String.valueOf(i));

			pageBtnNext.addThemeVariants(ButtonVariant.LUMO_ICON);
			pageBtnNext.setAriaLabel("Next Page");
			pageBtnNext.setId(String.valueOf(i));
			pageBtnNext.setTooltipText("Go to page " + i);

			pageBtnNext.addClickListener(event -> {

				postContent.removeAll();
				// System.out.println("Current last: " + currentLastId);
				pageBtnNext.getParent().get().getChildren().forEach(item -> {
					if (item instanceof Button bt) {
						bt.setEnabled(true);
					}
				});

				pageBtnNext.setDisableOnClick(true);

				if (Integer.parseInt(event.getSource().getId().get()) < buttonSelected) {

					int diff = Math.abs(Integer.parseInt(event.getSource().getId().get()) - buttonSelected);
					buttonSelected = Integer.parseInt(event.getSource().getId().get());
					System.out.println("INIT buttonselected: " + buttonSelected + " button clicked "
							+ event.getSource().getId().get() + " diff: " + diff + " currentLastId " + currentLastId
							+ " postperpage: " + postsPerCurrentPage);

					System.out.println(currentLastId + " " + diff + " " + postsPerCurrentPage);
					int start = (currentLastId - (diff * 4) + postsPerCurrentPage) - 4;
					System.out.println("start: " + start);

					for (Post p : postRepo.getNextPageItemsByPreviousID(start, !test.isEmpty(),
							test.toArray(new Integer[test.size()]))) {
						PostBox post1 = new PostBox(p.getTitle(), p.getSummary(), p.getBody(), p.getAuthor(),
								p.getDate());

						Hr hr = new Hr();
						postContent.add(post1, hr);
						currentLastId = p.getId();

					}
				} else {
					int diff = Math.abs(Integer.parseInt(event.getSource().getId().get()) - buttonSelected);
					System.out.println("ELSE: buttonselected: " + buttonSelected + " button clicked "
							+ event.getSource().getId().get() + " diff: " + diff + " currentLastId " + currentLastId
							+ " postperpage: " + postsPerCurrentPage);
					buttonSelected = Integer.parseInt(event.getSource().getId().get());
					int start = 0;
					if (diff == 1 || diff == 0)
						start = currentLastId;// (diff * 4)
					else
						start = currentLastId + ((diff - 1) * 4);
					System.out.println("start: " + start);
					int pages = 0;
					for (Post p : postRepo.getNextPageItemsByPreviousID(start, !test.isEmpty(),
							test.toArray(new Integer[test.size()]))) {
						PostBox post1 = new PostBox(p.getTitle(), p.getSummary(), p.getBody(), p.getAuthor(),
								p.getDate());

						Hr hr = new Hr();
						postContent.add(post1, hr);
						currentLastId = p.getId();
						pages++;

					}
					postsPerCurrentPage = Math.abs((pages - 4));
					pages = 0;
					System.out.println("currentpage psots: " + postsPerCurrentPage);
				}
			});
			footer.add(pageBtnNext);
		}
	}

}
