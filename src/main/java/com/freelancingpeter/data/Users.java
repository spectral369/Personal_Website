package com.freelancingpeter.data;

import java.io.Serializable;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.token.Sha512DigestUtils;

import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "users")
@EnableJpaAuditing
@EntityListeners(AuditingEntityListener.class)
public class Users implements Serializable {

	private static final long serialVersionUID = -962071896545861952L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@NotNull
	private String username;
	private String email;
	private Role role;
	private String passwordSalt;
	private String passwordHash;
	private String activationCode;
	private boolean active;

	public Users() {
	}

	public Users(String username, String email, String password, Role role) {
		this.username = username;
		this.email = email; // validatin will be on frontend
		this.role = role;
		this.passwordSalt = RandomStringUtils.secure().nextAlphanumeric(32);
		this.passwordHash = Sha512DigestUtils.shaHex(password + passwordSalt);
		this.activationCode = RandomStringUtils.secure().nextAlphanumeric(6);

	}

	public boolean checkPassword(String password) {
		return Sha512DigestUtils.shaHex(password + passwordSalt).equals(passwordHash);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasswordSalt() {
		return passwordSalt;
	}

	public void setPasswordSalt(String passwordSalt) {
		this.passwordSalt = passwordSalt;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
