package com.freelancingpeter.pages;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.shared.Tooltip;
import com.wontlost.ckeditor.Config;
import com.wontlost.ckeditor.Constants.EditorType;
import com.wontlost.ckeditor.VaadinCKEditor;
import com.wontlost.ckeditor.VaadinCKEditorBuilder;

public class PostBox extends VerticalLayout {

	private static final long serialVersionUID = 4093569057238674638L;

	private H3 titleH = null;
	private VerticalLayout con = null;
	private Div bodyPre = null;
	private Config config = null;
	private VaadinCKEditor editor = null;
	private HorizontalLayout bottom = null;
	private H4 AuthorL = null;
	private H4 dateLabel = null;

	public PostBox(String title, String summary, String body, String author, LocalDate date) {

		titleH = new H3(title);
		titleH.setWidthFull();
		titleH.setHeight("min-content");
		titleH.getStyle().set("overflow", "hidden");
		titleH.getStyle().set("text-overflow", "ellipsis");
		titleH.getStyle().set("white-space", "nowrap");

		con = new VerticalLayout();
		// new
		con.setMargin(false);
		con.setPadding(false);
		con.setSpacing(false);
		con.setWidthFull();

		// end new

		bodyPre = new Div();

		bodyPre.getStyle().set("width", "-webkit-fill-available");
		bodyPre.getStyle().set("padding", ".3em 0.5em .1em 2em");
		bodyPre.getStyle().set("overflow", "hidden");
		bodyPre.getStyle().set("display", "-webkit-box");
		bodyPre.getStyle().set("text-overflow", "ellipsis");
		bodyPre.getStyle().set("-webkit-line-clamp", "2");
		bodyPre.getStyle().set("-webkit-box-orient", "vertical");
		bodyPre.getStyle().set("word-wrap", "anywhere");
		bodyPre.getStyle().set("text-indent", "-1em");
		bodyPre.getStyle().set("white-space", "break-spaces");

		bodyPre.add(summary);
		con.add(bodyPre);

		config = new Config();
		config.setEditorToolBar(null);
		config.setBlockToolBar(null);
		editor = new VaadinCKEditorBuilder().with(builder -> {
			builder.editorData = body;
			builder.editorType = EditorType.INLINE;
			builder.width = "90%";
			builder.readOnly = true;
			builder.waitingTime = 5000;
			builder.ghsEnabled = true;
			builder.hideToolbar = true;
			builder.sync = false;
			builder.autosave = false;
			builder.config = config;
		}).createVaadinCKEditor();
		editor.getStyle().clear();
		editor.addClassName("readEditor");

		bottom = new HorizontalLayout();
		bottom.setMargin(false);
		bottom.setSpacing(false);
		bottom.setPadding(false);
		bottom.setWidthFull();
		bottom.setHeight("min-content");
		AuthorL = new H4(author);
		dateLabel = new H4(date.format(DateTimeFormatter.ISO_LOCAL_DATE));
		Tooltip.forComponent(dateLabel).withText("Click to collapse").withPosition(Tooltip.TooltipPosition.TOP_START);
		bottom.add(AuthorL, dateLabel);
		bottom.setFlexGrow(1, AuthorL);

		dateLabel.addClickListener(event -> {
			bodyPre.getStyle().set("overflow", "hidden");
			bodyPre.getStyle().set("display", "-webkit-box");
			con.remove(editor);
		});

		add(titleH, con, bottom);
		setAlignSelf(Alignment.END, dateLabel);
		setJustifyContentMode(JustifyContentMode.EVENLY);
		setMargin(false);
		setPadding(true);
		setSpacing(true);
		// setSizeFull();
		setWidthFull();
		bodyPre.addClickListener(event -> {
			bodyPre.getStyle().set("overflow", "none");
			bodyPre.getStyle().set("display", "inline-table");
			dateLabel.getStyle().set("text-decoration", "underline");
			con.add(editor);
		});
		titleH.addClickListener(event -> {
			bodyPre.getStyle().set("overflow", "none");
			bodyPre.getStyle().set("display", "inline-table");
			dateLabel.getStyle().set("text-decoration", "underline");
			con.add(editor);
		});
	}

}
