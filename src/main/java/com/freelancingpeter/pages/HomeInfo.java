package com.freelancingpeter.pages;

import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.server.StreamResource;

public class HomeInfo extends VerticalLayout {

	private static final long serialVersionUID = -1368882963984683737L;
	private VerticalLayout content;

	public HomeInfo() {

		content = new VerticalLayout();
		Span textcontent = new Span();

		StreamResource logoResource = new StreamResource("infoImg2.webp",
				() -> getClass().getResourceAsStream("/META-INF/resources/icons/infoImg2.webp"));
		
		Image image = new Image(logoResource, "infoLogo");
		image.getStyle().set("height", "3.7em");
		image.getStyle().set("margin-bottom", "-0.7em");
		
		textcontent.add(image);

		
		textcontent.add(
				"i, I'm Peter P., a DevOps engineer with a passion for automation, Linux, and innovative technologies. With "
						+ "expertise in Ansible, Bash, Jenkins CI/CD, Docker and various programming languages, as well as Debian-based Linux ecosystems, I enjoy "
						+ "tackling complex challenges and streamlining processes building utility applications, many for my own need.\n");
		textcontent.getStyle().set("text-align", "center");
		content.add(textcontent);

		Span textContent10 = new Span(
				"In my free time, I enjoy tinkering with Raspberry Pi 4 and Orange Pi 5B, exploring new possibilities serving websites and a range of services, including mail, matrix instance, and revolt chat and more.\n"
				+ "You can find my projects on Codeberg.org (https://codeberg.org/spectral369), where I showcase my skills in various technologies. I also manage several"
				+ " websites, including https://freelancingpeter.eu, https://diggdolma.com, and my ActivityPub instance, https://social-me.online."
				+ " When I'm not coding, you can find me mountain trekking, enjoying the great outdoors.");
		textContent10.getStyle().set("text-align", "center");

		content.add(textContent10);
		content.setAlignItems(Alignment.CENTER);

		content.setHeightFull();
		add(content);

	}

}
