package com.freelancingpeter.data;

import java.util.ArrayList;
import java.util.List;

import org.simplejavamail.api.email.Email;
import org.simplejavamail.api.mailer.Mailer;
import org.simplejavamail.api.mailer.config.TransportStrategy;
import org.simplejavamail.email.EmailBuilder;
import org.simplejavamail.mailer.MailerBuilder;
import org.springframework.stereotype.Service;

import com.freelancingpeter.account.LogoutView;
import com.freelancingpeter.pages.PostView;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.server.VaadinSession;

@Service
public class AuthService {

	public record AuthorizedRoute(String route, String name, Class<? extends Component> view) {

	}

	public class AuthException extends Exception {

		private static final long serialVersionUID = 1083465258809103331L;

	}

	private final UsersRepository userRepository;

	public AuthService(UsersRepository userRepository) {
		this.userRepository = userRepository;
	}

	public void authenticate(String username, String password) throws AuthException {
		Users user = userRepository.getByUsername(username);
		if (user != null && user.checkPassword(password) && user.isActive()) {
			VaadinSession.getCurrent().setAttribute(Users.class, user);
			createRoutes(user.getRole());
		} else {
			throw new AuthException();
		}
	}

	private void createRoutes(Role role) {
		getAuthorizedRoutes(role).stream().forEach(route -> {
			if (!RouteConfiguration.forSessionScope().getRoute(route.route).isPresent()) {
				RouteConfiguration.forSessionScope().setRoute(route.route, route.view);
			}

		});
	}

	public List<AuthorizedRoute> getAuthorizedRoutes(Role role) {
		var routes = new ArrayList<AuthorizedRoute>();

		if (role.equals(Role.ADMIN)) {
			routes.add(new AuthorizedRoute("post", "PostView", PostView.class));
			routes.add(new AuthorizedRoute("logout", "Logout", LogoutView.class));
		}

		return routes;
	}

	public void register(String username, String email, String password) {
		System.out.println(username + " " + email + " " + password);
		Users user = userRepository.save(new Users(username, email, password, Role.ADMIN)); // hardcode
		String text = "Freelancingpeter account acctivation url:\n\n" + AccountsInfo.getInstance().getSiteUrl()
				+ "/activate?code=" + user.getActivationCode() + "\n\nP.";
		Mailer mailer = MailerBuilder
				.withSMTPServer("spectral369fp.freelancingpeter.eu", 465, "site@freelancingpeter.eu",
						AccountsInfo.getInstance().getMailPassword())
				.withTransportStrategy(TransportStrategy.SMTPS).buildMailer();

		Email email_b = EmailBuilder.startingBlank().from("noreplay", "site@freelancingpeter.eu").to(username, email)
				.withSubject("Confirmation email").withPlainText(text).buildEmail();

		mailer.sendMail(email_b);
	}

	public void activate(String activationCode) throws AuthException {
		Users user = userRepository.getByActivationCode(activationCode);
		if (user != null) {
			user.setActive(true);
			userRepository.save(user);
		} else {
			throw new AuthException();
		}
	}

}
