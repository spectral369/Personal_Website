package com.freelancingpeter.data;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface PostsRepository extends JpaRepository<Post, Integer> {

	@Query(value = "from Post where (CASE WHEN :isstr IS true THEN id in :intarr ELSE TRUE END) order by id limit 4")
	List<Post> getFirstPageItems(@Param("isstr") boolean isstr, @Param("intarr") Integer[] intarr);

	@Query(value = "from Post where id > :id and id < (:id+5) AND (CASE WHEN :isstr IS true THEN id in :intarr ELSE TRUE END)")
	List<Post> getNextPageItemsByPreviousID(@Param("id") int id, @Param("isstr") boolean isstr,
			@Param("intarr") Integer[] intarr);

	@Query(value = "from Post where where title like %:searchstr% OR summary like %:searchstr%")
	List<Post> getSearchResults(@Param("searchstr") String searchstr);

}
