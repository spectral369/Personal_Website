package com.freelancingpeter.pages;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import com.freelancingpeter.data.PostsRepository;
import com.freelancingpeter.sides.Intrests;
import com.freelancingpeter.sides.SteamSide;
import com.freelancingpeter.utils.Utils;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.progressbar.ProgressBar;
import com.vaadin.flow.router.PageTitle;

@PageTitle("Freelancing Peter")
public class Home extends FlexLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 315375221544904587L;

	Button steamDisplay = null;
	VerticalLayout content = null;
	boolean isSteamDisplayed = false;
	SteamSide steamSide = null;

	Button tootsDisplay = null;
	boolean isTootsDisplayed = false;
	Intrests intrestsSide = null;
	private int progressValue = 4;
	private AtomicInteger info = null;
	private HomeInfo homeInfo = null;
	private HomeBlog homeBlog = null;

	public Home(PostsRepository postRepo) {

		info = new AtomicInteger(0);
		content = new VerticalLayout();
		content.setHeightFull();
		content.setAlignItems(Alignment.CENTER);
		content.getStyle().set("border-right", "ridge");
		content.getStyle().set("border-left", "ridge");

		steamSide = new SteamSide();
		intrestsSide = new Intrests();
		steamDisplay = new Button(new Icon(VaadinIcon.ARROW_CIRCLE_RIGHT_O));
		tootsDisplay = new Button(new Icon(VaadinIcon.ARROW_CIRCLE_LEFT_O));
		add(steamDisplay);
		steamDisplay.getStyle().set("display", "none");
		steamDisplay.getStyle().set("display", "none");
		steamDisplay.addClickListener(e -> {
			isSteamDisplayed = !isSteamDisplayed;
			Utils.setLeftListener(steamSide, isSteamDisplayed, intrestsSide, steamDisplay, content);
			if (isTootsDisplayed) {
				isTootsDisplayed = !isTootsDisplayed;
				tootsDisplay.setIcon(new Icon(VaadinIcon.ARROW_CIRCLE_LEFT_O));
			}
		});
		add(steamSide);

		H2 h2title = new H2("Welcome");
		content.add(h2title);
		content.setMargin(false);
		content.setPadding(false);

		// test
		VerticalLayout swBtnnParrent = new VerticalLayout();
		swBtnnParrent.setPadding(false);
		swBtnnParrent.setMargin(false);
		swBtnnParrent.setSpacing(false);
		swBtnnParrent.setWidthFull();
		swBtnnParrent.setAlignItems(Alignment.CENTER);
		HorizontalLayout btnLayout = new HorizontalLayout();
		btnLayout.setAlignItems(Alignment.CENTER);

		Button Info = new Button("Info", VaadinIcon.FILE_TEXT_O.create());
		Info.setDisableOnClick(true);
		Info.setEnabled(false);
		Button Blog = new Button("Blog", VaadinIcon.CLIPBOARD_TEXT.create());
		Blog.setEnabled(true);
		Blog.setDisableOnClick(true);
		ProgressBar progressBar = new ProgressBar(0d, 4d);
		progressBar.setValue(4d);
		progressBar.setWidth("3em");

		Info.addClickListener(event -> {
			new Thread(() -> {
				for (int y = 0; y <= 3; y++) {
					try {
						TimeUnit.MILLISECONDS.sleep(400);
						getUI().ifPresent(ui -> ui.access(() -> {
							progressBar.setValue(Double.valueOf(++progressValue));
						}));

					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}

				}

				getUI().ifPresent(ui -> ui.access(() -> {
					Blog.setEnabled(true);

					swi(info);
				}));
			}).start();
		});

		Blog.addClickListener(event -> {
			new Thread(() -> {
				for (int y = 4; y >= 1; y--) {
					try {
						TimeUnit.MILLISECONDS.sleep(400);
						getUI().ifPresent(ui -> ui.access(() -> {
							progressBar.setValue(Double.valueOf(--progressValue));
						}));

					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}

				}

				getUI().ifPresent(ui -> ui.access(() -> {
					Info.setEnabled(true);
					swi(info);
				}));
			}).start();
		});

		btnLayout.add(Info, progressBar, Blog);

		swBtnnParrent.add(btnLayout);
		content.add(swBtnnParrent);
		homeInfo = new HomeInfo();
		homeBlog = new HomeBlog(postRepo);
		content.add(homeInfo);

		Scroller scroller = new Scroller(content);
		scroller.setScrollDirection(Scroller.ScrollDirection.VERTICAL);
		scroller.setSizeFull();
		scroller.getStyle().set("padding-bottom", "0px !important");
		scroller.getStyle().set("margin-bottom", "0px !important");
		add(scroller);

		add(tootsDisplay);
		tootsDisplay.getStyle().set("display", "none");
		tootsDisplay.addClickListener(e -> {
			isTootsDisplayed = !isTootsDisplayed;
			Utils.setRightListener(steamSide, isTootsDisplayed, intrestsSide, tootsDisplay, content);
			if (isSteamDisplayed) {
				isSteamDisplayed = !isSteamDisplayed;
				steamDisplay.setIcon(new Icon(VaadinIcon.ARROW_CIRCLE_RIGHT_O));
			}
		});
		add(intrestsSide);

		UI.getCurrent().getPage().addBrowserWindowResizeListener(e -> {

			Utils.resizeListenerMethod(e, steamDisplay, steamSide, tootsDisplay, intrestsSide);
			scroller.setHeight(0.78 * (e.getHeight()) + "px");

		});

		setJustifyContentMode(JustifyContentMode.CENTER);
		setSizeFull();

		UI.getCurrent().getPage().retrieveExtendedClientDetails(details -> {
			Utils.resize(details.getScreenWidth(), steamDisplay, tootsDisplay, steamSide, intrestsSide);
			// scroller.setHeight(0.76 * (details.getWindowInnerHeight()) + "px");
			scroller.setHeight(0.78 * details.getWindowInnerHeight() + "px");
		});
		this.getStyle().set("padding-bottom", "0px !important");
		this.getStyle().set("margin-bottom", "0px !important");
		expand(content);

	}

	private void swi(AtomicInteger info) {
		switch (info.getAcquire()) {
		case 0 -> {
			if (content.getChildren().filter(item -> item.equals(homeInfo)).findAny().isPresent())
				content.remove(homeInfo);

			content.add(homeBlog);
			if (Utils.isMobileDevice()) {
				steamSide.setVisible(false);
				intrestsSide.setVisible(false);
				steamDisplay.setVisible(false);
				tootsDisplay.setVisible(false);
			}
			info.set(1);

		}

		case 1 -> {
			if (content.getChildren().filter(item -> item.equals(homeBlog)).findAny().isPresent())
				content.remove(homeBlog);

			content.add(homeInfo);
			if (Utils.isMobileDevice()) {
				steamSide.setVisible(true);
				intrestsSide.setVisible(true);
				steamDisplay.setVisible(true);
				tootsDisplay.setVisible(true);
			}
			info.set(0);

		}
		default -> System.out.println("switcher error - impossible");
		}
	}

}