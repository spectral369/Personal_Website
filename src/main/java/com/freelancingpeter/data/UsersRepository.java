package com.freelancingpeter.data;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<Users, Integer> {

	Users getByUsername(String username);

	Users getByActivationCode(String activationCode);
}
