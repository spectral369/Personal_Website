package com.freelancingpeter.workers;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.cookie.StandardCookieSpec;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.HttpHeaders;
import org.apache.hc.core5.http.message.BasicHeader;

import com.freelancingpeter.data.Data;

public class SteamLoading implements Serializable {

	private static final long serialVersionUID = -3993897546567437261L;

	private final String key;
	private final String steamId;
	private HttpGet httpget1 = null;
	private HttpGet httpget2 = null;
	private HttpGet httpget3 = null;
	// private HttpClient client = null;
	// private BasicHttpContext localContext = null;
	private String userInfo = null;
	private String gameStats = null;
	private String recentGames = null;
	protected int recentGamesCount = 0;

	public SteamLoading(String key, String id) {
		this.key = key;
		this.steamId = id;
		System.out.println("runSteam");
		Data.INSTANCE.clearSteam();
		Data.INSTANCE.setSteam(getUserInfo());
		Data.INSTANCE.setSteam(getAllGames());
		Data.INSTANCE.setSteam(getRecentGames());
		System.out.println("doneSteam");
		userInfo = null;
		gameStats = null;
		recentGames = null;

	}

	protected synchronized String getRecentGames() {

		if (recentGames == null) {

			CloseableHttpClient client = HttpClients.custom()
					.setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(StandardCookieSpec.RELAXED).build())
					.build();

			httpget1 = new HttpGet("https://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/" + "?key="
					+ key + "&steamid=" + steamId + "&format=json");
			httpget1.setHeader(new BasicHeader("User-Agent",
					"Mozilla/5.0 (Windows NT 10.0; rv:112.0) Gecko/20100101 Firefox/112.0"));
			httpget1.addHeader("Accept-Language", "en-US,en;q=0.5");
			httpget1.addHeader("Accept-Charset", "application/x-www-form-urlencoded; charset=UTF-8");
			httpget1.addHeader(HttpHeaders.CONTENT_TYPE, "application/json");

			try {

				client.execute(httpget1, (response) -> {
					HttpEntity entity = response.getEntity();
					recentGames = getASCIIContentFromEntity(entity);
					setNoGames();

					return null;
				});

			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		return recentGames;
	}

	private void setNoGames() {
		try {
			String a = recentGames.substring(recentGames.indexOf("total_count") + 12);
			Data.INSTANCE.setRecentGames(Integer.parseInt(a.substring(1, 2)));
			if (Data.INSTANCE.getRecentGames() > 3)
				Data.INSTANCE.setRecentGames(3);
		} catch (Exception x) {
			Data.INSTANCE.setRecentGames(0);
		}
	}

	protected synchronized String getAllGames() {

		if (gameStats == null) {

			CloseableHttpClient client = HttpClients.custom()
					.setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(StandardCookieSpec.RELAXED).build())
					.build();

			httpget2 = new HttpGet("https://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=" + key
					+ "&steamid=" + steamId + "&include_appinfo=1&format=json&include_played_free_games=1");
			httpget2.setHeader(new BasicHeader("User-Agent",
					"Mozilla/5.0 (Windows NT 10.0; rv:112.0) Gecko/20100101 Firefox/112.0"));
			httpget2.addHeader("Accept-Language", "en-US,en;q=0.5");
			httpget2.addHeader("Accept-Charset", "application/x-www-form-urlencoded; charset=UTF-8");
			httpget2.addHeader(HttpHeaders.CONTENT_TYPE, "application/json");

			try {

				client.execute(httpget2, (response) -> {
					HttpEntity entity = response.getEntity();
					gameStats = getASCIIContentFromEntity(entity);

					return null;
				});

			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		return gameStats;
	}

	protected synchronized String getUserInfo() {

		if (userInfo == null) {

			CloseableHttpClient client = HttpClients.custom()
					.setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(StandardCookieSpec.RELAXED).build())
					.build();

			httpget3 = new HttpGet("https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=" + key
					+ "&steamids=" + steamId);
			httpget3.setHeader(new BasicHeader("User-Agent",
					"Mozilla/5.0 (Windows NT 10.0; rv:112.0) Gecko/20100101 Firefox/112.0"));
			httpget3.addHeader("Accept-Language", "en-US,en;q=0.5");
			httpget3.addHeader("Accept-Charset", "application/x-www-form-urlencoded; charset=UTF-8");
			httpget3.addHeader(HttpHeaders.CONTENT_TYPE, "application/json");

			try {

				client.execute(httpget3, (response) -> {
					HttpEntity entity = response.getEntity();
					userInfo = getASCIIContentFromEntity(entity);

					return null;
				});

			} catch (IOException e) {

				e.printStackTrace();
			}
		}

		return userInfo;
	}

	protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
		InputStream in = entity.getContent();
		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);
			if (n > 0)
				out.append(new String(b, 0, n));
		}
		return out.toString();
	}

	protected String getJsonValue(String key, String json, int order) {
		String value = null;
		if (order == 1)
			json = json.substring(json.indexOf(key) + 1);
		if (order == 2) {
			json = json.substring(json.indexOf(key) + 1);
			json = json.substring(json.indexOf(key) + 1);
		}
		try {
			int index1 = json.indexOf(key);
			int index2 = json.substring(index1).indexOf(":") + 1;
			int index3 = json.substring(index1 + index2).indexOf(",");
			value = json.substring(index1 + index2, index1 + index2 + index3);
			value = value.replaceAll("[\\s+\"]", "");

		} catch (Exception e) {
			return "0";
		}
		if (value != null) {
			if (value.contains(":")) {
				if (!value.contains("akamai"))
					value = value.replaceAll(":", " ");
			}
		}
		return value;
	}

}
