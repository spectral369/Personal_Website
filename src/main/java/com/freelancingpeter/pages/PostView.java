package com.freelancingpeter.pages;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.cookie.StandardCookieSpec;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.HttpHeaders;
import org.apache.hc.core5.http.message.BasicHeader;
import org.apache.hc.core5.net.URIBuilder;

import com.freelancingpeter.data.AccountsInfo;
import com.freelancingpeter.data.Post;
import com.freelancingpeter.data.PostsRepository;
import com.freelancingpeter.data.Users;
import com.freelancingpeter.utils.Utils;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.server.VaadinSession;
import com.wontlost.ckeditor.Constants.EditorType;
import com.wontlost.ckeditor.Constants.ThemeType;
import com.wontlost.ckeditor.VaadinCKEditor;
import com.wontlost.ckeditor.VaadinCKEditorBuilder;

@PageTitle("Post")
//@Layout
//@Route("post")
public class PostView extends VerticalLayout {

	private static final long serialVersionUID = 59918328547892136L;

	private H2 titleTop = null;
	private VerticalLayout content = null;
	private TextField title = null;
	private TextArea ta = null;
	private VaadinCKEditor decoupledEditor = null;
	private Button createPost = null;

	public PostView(PostsRepository postRepo) {
		setId("post-view");
		titleTop = new H2("Posts manager");

		content = new VerticalLayout();
		content.setSizeFull();
		content.setAlignItems(Alignment.CENTER);
		title = new TextField("Post Title");
		title.setRequired(true);
		title.setRequiredIndicatorVisible(true);
		title.setWidthFull();

		ta = new TextArea("Sumary");
		ta.getStyle().set("box-sizing", "border-box");
		ta.setSizeFull();
		ta.setMaxLength(244);
		ta.setMinRows(6);

		decoupledEditor = new VaadinCKEditorBuilder().with(builder -> {
			builder.editorType = EditorType.DECOUPLED;
			builder.theme = ThemeType.DARK;
		}).createVaadinCKEditor();
		decoupledEditor.setRequiredIndicatorVisible(true);
		decoupledEditor.setHeight("65vh");

		createPost = new Button("Create Post");
		createPost.setDisableOnClick(true);
		createPost.addClickListener(event -> {
			Post p = new Post(title.getValue().trim(), ta.getValue().trim(), decoupledEditor.getValue().trim(),
					VaadinSession.getCurrent().getAttribute(Users.class).getUsername(), LocalDate.now());
			postOnMastodon();
			postRepo.save(p);
			Notification.show("Post created...", 4000, Position.BOTTOM_END);
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					content.getUI().get().access(() -> {
						content.getUI().get().getPage().setLocation("/");
					});
				}
			}, 5000);

		});

		content.add(title, ta, decoupledEditor, createPost);
		add(titleTop, content);

		setSizeFull();
		setAlignItems(Alignment.CENTER);
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
	}

	private void postOnMastodon() {

		CloseableHttpClient client = HttpClients.custom()
				.setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(StandardCookieSpec.RELAXED).build())
				.build();

		HttpPost request = new HttpPost("https://mastodon.social/api/v1/statuses");
		request.setHeader(
				new BasicHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:112.0) Gecko/20100101 Firefox/112.0"));
		request.addHeader("Accept-Language", "en-US,en;q=0.5");
		request.addHeader("Accept-Charset", "application/x-www-form-urlencoded; charset=UTF-8");
		request.addHeader("Authorization", "Bearer " + AccountsInfo.getInstance().getMASTODONTOKEN());
		request.addHeader(HttpHeaders.CONTENT_TYPE, "application/json");

		try {

			URI uri = new URIBuilder(request.getUri())
					.addParameter("spoiler_text",
							"New Post on https://freelancingpeter.eu\n" + title.getValue().trim()
									+ "\nThis toot is automated !")
					.addParameter("status", ta.getValue().trim()).addParameter("visibility", "public")
					.addParameter("language", "en").build();
			request.setUri(uri);

			client.execute(request, (response) -> {
				HttpEntity entity = response.getEntity();

				String json = Utils.getASCIIContentFromEntity(entity);
				System.out.println(json);
				return null;
			});

		} catch (IOException | URISyntaxException e) {

			System.out.println(e.getMessage());
		}

	}

}
