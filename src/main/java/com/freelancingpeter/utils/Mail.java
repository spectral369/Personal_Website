package com.freelancingpeter.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.simplejavamail.api.email.Email;
import org.simplejavamail.api.mailer.Mailer;
import org.simplejavamail.api.mailer.config.TransportStrategy;
import org.simplejavamail.email.EmailBuilder;
import org.simplejavamail.mailer.MailerBuilder;

import com.freelancingpeter.data.AccountsInfo;

public class Mail {

	public Mail(String name, String aemail, String amessage) {
		try {
			Mailer mailer = MailerBuilder
					.withSMTPServer("spectral369fp.freelancingpeter.eu", 465, "site@freelancingpeter.eu",
							AccountsInfo.getInstance().getMailPassword())
					// .withProperty("mail.smtp.sendpartial", "true")
					// .withProperty("mail.smtp.starttls.enable", "true")
					.withTransportStrategy(TransportStrategy.SMTPS)
					// .withDebugLogging(true)
					.buildMailer();

			Email email = EmailBuilder.startingBlank().from("Mail1 from " + name, "site@freelancingpeter.eu")
					.to("spectral369", "spectral369@freelancingpeter.eu")
					.withSubject("mail1 from " + name + " " + new SimpleDateFormat("MM-dd-yyyy").format(new Date()))
					.withPlainText("message: " + amessage + " " + aemail).buildEmail();
			// mailer.testConnection();

			mailer.sendMail(email);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}