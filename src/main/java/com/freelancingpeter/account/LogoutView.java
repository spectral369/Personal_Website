package com.freelancingpeter.account;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.server.VaadinSession;

//@Route("logout")
@PageTitle("Logout")
public class LogoutView extends Composite<VerticalLayout> {

	private static final long serialVersionUID = -4736361481205342089L;

	public LogoutView() {
		UI.getCurrent().getPage().setLocation("login");
		VaadinSession.getCurrent().getSession().invalidate();
		VaadinSession.getCurrent().close();
	}

}