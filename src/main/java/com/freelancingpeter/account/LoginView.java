package com.freelancingpeter.account;

import java.util.Timer;
import java.util.TimerTask;

import com.freelancingpeter.data.AuthService;
import com.freelancingpeter.data.Users;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.VaadinSession;

@Route(value = "login")
@PageTitle("Login")
public class LoginView extends VerticalLayout implements RouterLayout {

	private static final long serialVersionUID = -1559995583844835801L;

	public LoginView(AuthService authService) {
		setId("login-view");
		setEnabled(false);
		System.out.println("in Login: " + VaadinSession.getCurrent().getAttribute(Users.class));
		if (VaadinSession.getCurrent().getAttribute(Users.class) != null) {
			UI.getCurrent().getPage().setLocation("post");
		}
		setEnabled(true);
		LoginForm loginForm = new LoginForm();

		loginForm.getElement().getThemeList().add("dark");
		loginForm.addLoginListener(levent -> {
			try {
				authService.authenticate(levent.getUsername(), levent.getPassword());
				Notification.show("Auth successfull.", 3000, Position.BOTTOM_END);

				Timer timer = new Timer();
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						loginForm.getUI().get().access(() -> {
							UI.getCurrent().getPage().setLocation("post");
						});
					}
				}, 3000);

			} catch (AuthService.AuthException e) {
				System.out.println("failed to login: " + e);
			}

		});
		add(loginForm);
		setAlignItems(Alignment.CENTER);
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		RouteConfiguration.forSessionScope().removeRoute("register");
		RouteConfiguration.forSessionScope().setRoute("register", RegisterView.class);

		add(new RouterLink("Register", RegisterView.class));

	}

}
