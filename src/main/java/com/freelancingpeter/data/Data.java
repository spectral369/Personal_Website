package com.freelancingpeter.data;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public enum Data {
	INSTANCE();

	private LinkedList<String> toots;
	private LinkedList<String> steam;
	private Map<String, String> lol;
	private int recentGamesCount = 0;

	public void setMasto(List<String> tootss) {
		if (tootss != null)
			toots.addAll(tootss);
	}

	public void setSteam(String steamm) {
		if (steamm != null)
			steam.add(steamm);
	}

	public void setLol(Map<String, String> map) {
		if (map != null)
			lol.putAll(map);
	}

	public List<String> getToots() {
		return toots;
	}

	public void setRecentGames(int noGames) {
		recentGamesCount = noGames;
	}

	public int getRecentGames() {
		return recentGamesCount;
	}

	public List<String> getSteam() {
		return steam;
	}

	public Map<String, String> getLol() {
		return lol;
	}

	public void clearLists() {
		steam.clear();
		toots.clear();
		lol.clear();
	}

	public void clearSteam() {
		steam.clear();
	}

	public void clearMasto() {
		toots.clear();
	}

	public void clearLol() {
		lol.clear();
	}

	// Constructor
	Data() {
		toots = new LinkedList<String>();
		steam = new LinkedList<String>();
		lol = new HashMap<String, String>();
	}
}