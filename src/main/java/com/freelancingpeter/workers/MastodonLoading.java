
package com.freelancingpeter.workers;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.cookie.StandardCookieSpec;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.HttpHeaders;
import org.apache.hc.core5.http.message.BasicHeader;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import com.freelancingpeter.data.Data;

public class MastodonLoading implements Serializable {

	private static final long serialVersionUID = 607609176832697852L;

	private final String ACCESSTOKEN;

	private String ACCOUNTID;

	List<String> toots = null;
	private String json = null;

	public MastodonLoading(String token, String id) {
		this.ACCESSTOKEN = token;
		this.ACCOUNTID = id;
		System.out.println("runMastodon");
		Data.INSTANCE.clearMasto();
		Data.INSTANCE.setMasto(getToots());
		System.out.println("doneMastodon");
		toots = null;
		json = null;
	}

	protected synchronized List<String> getToots() {
		if (toots == null) {

			toots = new LinkedList<String>();

			CloseableHttpClient client = HttpClients.custom()
					.setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(StandardCookieSpec.RELAXED).build())
					.build();

			HttpGet request = new HttpGet("https://mastodon.social/api/v1/accounts/" + ACCOUNTID + "/statuses");
			request.setHeader(new BasicHeader("User-Agent",
					"Mozilla/5.0 (Windows NT 10.0; rv:112.0) Gecko/20100101 Firefox/112.0"));
			request.addHeader("Accept-Language", "en-US,en;q=0.5");
			request.addHeader("Accept-Charset", "application/x-www-form-urlencoded; charset=UTF-8");
			request.addHeader("exclude_replies", "true");
			request.addHeader("Authorization", "Bearer " + ACCESSTOKEN);
			request.addHeader(HttpHeaders.CONTENT_TYPE, "application/json");

			try {

				client.execute(request, (response) -> {
					HttpEntity entity = response.getEntity();

					json = getASCIIContentFromEntity(entity);
					return null;
				});

			} catch (IOException e) {

				System.out.println(e.getMessage());
			}
		}

		try {
			toots.clear();
			if (!json.isBlank() || !json.isEmpty()) {

				JSONArray myArray = new JSONArray(json);

				for (int i = 0; i < myArray.length(); i++) {
					JSONObject obj = myArray.getJSONObject(i);
					JSONArray tag = obj.getJSONArray("tags");
					if (tag.toString().contains("name"))
						toots.add(Jsoup.parse(obj.get("content").toString()).text());
				}

			}
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
		}

		return toots;
	}

	protected synchronized String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();
		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);
			if (n > 0)
				out.append(new String(b, 0, n));
		}
		return out.toString();
	}

}
