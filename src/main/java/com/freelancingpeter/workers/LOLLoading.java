package com.freelancingpeter.workers;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.cookie.StandardCookieSpec;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.HttpHeaders;
import org.apache.hc.core5.http.message.BasicHeader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freelancingpeter.data.AccountsInfo;
import com.freelancingpeter.data.Data;

public class LOLLoading implements Serializable {

	private static final long serialVersionUID = -7976308675358079705L;

	private final String key;
	private final String id;
	private Map<String, String> info = new HashMap<String, String>();
	private final DecimalFormat df = new DecimalFormat("0.00");
	private boolean isKeyOK = true;

	public LOLLoading(String key, String id) {
		this.key = key;
		this.id = id;
		System.out.println("runLoL");
		getNameLevel();
		if (isKeyOK) {
			getName();
			getMatch();
			getMatchInfo();
			getRankInfo();
		}
		Data.INSTANCE.setLol(info);
		System.out.println("doneLoL");
	}

	protected synchronized void getNameLevel() {

		CloseableHttpClient client = HttpClients.custom()
				.setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(StandardCookieSpec.RELAXED).build())
				.build();

		HttpGet request = new HttpGet("https://eun1.api.riotgames.com/lol/summoner/v4/summoners/" + id);
		request.setHeader(
				new BasicHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:124.0) Gecko/20100101 Firefox/124.0"));
		request.addHeader("Accept-Language", "en-US,en;q=0.5");
		request.addHeader("Accept-Charset", "application/x-www-form-urlencoded; charset=UTF-8");
		request.addHeader("X-Riot-Token", key);
		request.addHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		try {

			client.execute(request, (response) -> {
				HttpEntity entity = response.getEntity();
				String json = getASCIIContentFromEntity(entity);
				if (!json.isBlank() || !json.isEmpty()) {
					JSONObject data = null;
					try {
						data = new JSONObject(json);

						// info.put("name", "Kukailimoku");
						info.put("summonerLevel", data.get("summonerLevel").toString());
						info.put("profileIconId", data.get("profileIconId").toString());
						info.put("puuid", data.getString("puuid"));
					} catch (JSONException e) {

						e.printStackTrace();
					}
				}
				return null;
			});

		} catch (Exception e) {
			isKeyOK = false;
			System.out.println(e.getLocalizedMessage());
		}

	}

	protected synchronized void getName() {

		CloseableHttpClient client = HttpClients.custom()
				.setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(StandardCookieSpec.RELAXED).build())
				.build();

		HttpGet request = new HttpGet(
				"https://europe.api.riotgames.com/riot/account/v1/accounts/by-puuid/" + info.get("puuid"));
		request.setHeader(
				new BasicHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:124.0) Gecko/20100101 Firefox/124.0"));
		request.addHeader("Accept-Language", "en-US,en;q=0.5");
		request.addHeader("Accept-Charset", "application/x-www-form-urlencoded; charset=UTF-8");
		request.addHeader("X-Riot-Token", key);
		request.addHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		try {

			client.execute(request, (response) -> {
				HttpEntity entity = response.getEntity();
				String json = getASCIIContentFromEntity(entity);
				if (!json.isBlank() || !json.isEmpty()) {
					JSONObject data = null;
					try {
						data = new JSONObject(json);

						info.put("name", data.get("gameName").toString());
						info.put("tagline", data.get("tagLine").toString());
					} catch (JSONException e) {

						e.printStackTrace();
					}
				}
				return null;
			});

		} catch (Exception e) {
			isKeyOK = false;
			System.out.println(e.getLocalizedMessage());
		}

	}

	protected synchronized void getMatch() {

		CloseableHttpClient client = HttpClients.custom()
				.setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(StandardCookieSpec.RELAXED).build())
				.build();

		HttpGet request = new HttpGet("https://europe.api.riotgames.com/lol/match/v5/matches/by-puuid/"
				+ info.get("puuid") + "/ids?start=0&count=3");
		request.setHeader(
				new BasicHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:124.0) Gecko/20100101 Firefox/124.0"));
		request.addHeader("Accept-Language", "en-US,en;q=0.5");
		request.addHeader("Accept-Charset", "application/x-www-form-urlencoded; charset=UTF-8");
		request.addHeader("X-Riot-Token", key);
		request.addHeader(HttpHeaders.CONTENT_TYPE, "application/json");

		try {
			client.execute(request, (response) -> {
				HttpEntity entity = response.getEntity();
				String json = getASCIIContentFromEntity(entity);
				if (!json.isBlank() || !json.isEmpty()) {
					JSONArray data = null;
					try {
						data = new JSONArray(json);

						info.put("match01", data.getString(0));
						info.put("match02", data.getString(1));
						info.put("match03", data.getString(2));
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				return null;
			});

		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
		}

	}

	protected synchronized void getRankInfo() {

		CloseableHttpClient client = HttpClients.custom()
				.setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(StandardCookieSpec.RELAXED).build())
				.build();

		HttpGet request = new HttpGet("https://eun1.api.riotgames.com/lol/league/v4/entries/by-summoner/" + id);
		request.setHeader(
				new BasicHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:112.0) Gecko/20100101 Firefox/112.0"));
		request.addHeader("Accept-Language", "en-US,en;q=0.5");
		request.addHeader("Accept-Charset", "application/x-www-form-urlencoded; charset=UTF-8");
		request.addHeader("X-Riot-Token", key);
		request.addHeader(HttpHeaders.CONTENT_TYPE, "application/json");

		try {

			client.execute(request, (response) -> {
				HttpEntity entity = response.getEntity();
				String json = getASCIIContentFromEntity(entity);
				if (!json.isBlank() || !json.isEmpty()) {
					JSONArray data;
					try {
						data = new JSONArray(json);

						for (int i = 0; i < data.length(); i++) {
							if (data.getJSONObject(i).get("queueType").toString().contains("SOLO")) {
								info.put("tier", data.getJSONObject(i).get("tier").toString());
								info.put("rank", data.getJSONObject(i).get("rank").toString());
							}
						}
					} catch (JSONException e) {

						e.printStackTrace();
					}

				}
				return null;
			});

		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
		}

	}

	protected synchronized void getMatchInfo() {

		CloseableHttpClient client = HttpClients.custom()
				.setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(StandardCookieSpec.RELAXED).build())
				.build();

		HttpGet request = new HttpGet("https://europe.api.riotgames.com/lol/match/v5/matches/" + info.get("match01"));
		request.setHeader(
				new BasicHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:112.0) Gecko/20100101 Firefox/112.0"));
		request.addHeader("Accept-Language", "en-US,en;q=0.5");
		request.addHeader("Accept-Charset", "application/x-www-form-urlencoded; charset=UTF-8");
		request.addHeader("X-Riot-Token", key);
		request.addHeader(HttpHeaders.CONTENT_TYPE, "application/json");

		try {

			client.execute(request, (response) -> {
				HttpEntity entity = response.getEntity();
				String json = getASCIIContentFromEntity(entity);
				if (!json.isBlank() || !json.isEmpty()) {
					JSONObject data;
					try {
						data = new JSONObject(json);

						JSONObject infoLol = data.getJSONObject("info");
						JSONArray champInfo = infoLol.getJSONArray("participants");
						JSONObject myObj = null;
						for (int i = 0; i < champInfo.length(); i++) {
							if (champInfo.getJSONObject(i).getString("summonerId")
									.equals(AccountsInfo.getInstance().getLOLID())) {
								myObj = champInfo.getJSONObject(i);
								break;
							}
						}
						info.put("gameEndTimestamp", String.valueOf(infoLol.getLong("gameEndTimestamp")));
						info.put("championName", myObj.getString("championName"));
						info.put("win", Boolean.valueOf(myObj.getBoolean("win")).toString());
						info.put("kda", String.valueOf(df.format(myObj.getJSONObject("challenges").getDouble("kda"))));
					} catch (JSONException e) {

						e.printStackTrace();
					}
				}
				return null;
			});

		} catch (Exception e) {
			System.out.println("getmatchInfo: " + e.getLocalizedMessage());
		}

	}

	protected synchronized String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();
		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);
			if (n > 0)
				out.append(new String(b, 0, n));
		}
		return out.toString();
	}

}
