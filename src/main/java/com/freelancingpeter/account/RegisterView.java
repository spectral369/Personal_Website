package com.freelancingpeter.account;

import java.util.Timer;
import java.util.TimerTask;

import com.freelancingpeter.data.AccountsInfo;
import com.freelancingpeter.data.AuthService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;

@Route("register")
public class RegisterView extends VerticalLayout {

	private static final long serialVersionUID = -5538136777498311556L;

	private final AuthService authService;

	public RegisterView(AuthService authService) {
		this.authService = authService;
		TextField username = new TextField("Username");
		EmailField email = new EmailField("Email");
		PasswordField password1 = new PasswordField("Password");
		PasswordField password2 = new PasswordField("Confirm password");

		H2 h2 = new H2("Register");
		Button register = new Button("Send");
		add(h2, username, email, password1, password2, register);
		if (AccountsInfo.getInstance().getRegisterOpen()) {
			register.addClickListener(event -> register(username.getValue().trim(), email.getValue().trim(),
					password1.getValue().trim(), password2.getValue().trim(), this));
		} else {
			register.setEnabled(false);
			register.setTooltipText("Registration is closed !");
		}
		setSizeFull();
		setAlignItems(Alignment.CENTER);
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
	}

	private void register(String username, String email, String password1, String password2, VerticalLayout vl) {
		if (username.trim().isEmpty()) {
			Notification.show("Enter a username");
		} else if (email.trim().isEmpty()) {
			Notification.show("Enter email");
		} else if (password1.isEmpty()) {
			Notification.show("Enter a password");
		} else if (!password1.equals(password2)) {
			Notification.show("Passwords don't match");
		} else {
			authService.register(username, email, password1);
			Notification.show("Check your email.", 3000, Position.BOTTOM_END);

			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					vl.getUI().get().access(() -> {
						vl.getUI().get().getPage().setLocation("login");
					});
				}
			}, 5000);
		}
	}

}
