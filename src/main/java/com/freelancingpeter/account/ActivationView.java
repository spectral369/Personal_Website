package com.freelancingpeter.account;

import java.util.List;
import java.util.Map;

import com.freelancingpeter.data.AuthService;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;

@Route("activate")
public class ActivationView extends VerticalLayout implements BeforeEnterObserver {

	private static final long serialVersionUID = 2764501714974039140L;

	private final AuthService authService;

	public ActivationView(AuthService authService) {
		this.authService = authService;
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		try {
			Map<String, List<String>> params = event.getLocation().getQueryParameters().getParameters();
			String code = params.get("code").get(0);
			authService.activate(code);
			this.add(new Text("Account activated."), new RouterLink("Login", LoginView.class));
		} catch (AuthService.AuthException e) {
			this.add(new Text("Invalid link."));
		}
	}

}
