package com.freelancingpeter.main;

import java.io.PrintStream;
import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Consumer;

import org.apache.tomcat.util.http.Rfc6265CookieProcessor;
import org.apache.tomcat.util.http.SameSiteCookies;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.embedded.tomcat.TomcatContextCustomizer;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.boot.web.servlet.server.CookieSameSiteSupplier;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.DefaultCsrfToken;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.util.WebUtils;

import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.component.page.Meta;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.server.AppShellSettings;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.SessionCookieConfig;
import jakarta.servlet.SessionTrackingMode;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * The entry point of the Spring Boot application.
 *
 * Use the @PWA annotation make the application installable on phones, tablets
 * and some desktop browsers.
 *
 */
@SpringBootApplication
@Push
@Theme(value = "freelancingpeter", variant = Lumo.DARK)
@PWA(name = "Freelancing Peter", shortName = "F.P", description = "Freelancingpeter - Personal Web Site", iconPath = "icons/icon.png", offlineResources = {
		"icons/icon.png" })
@Meta(name = "author", content = "spectral369")
@Meta(name = "title", content = "Freelancing Peter")
@Viewport("width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes")
@ComponentScan(basePackages = "com.freelancingpeter.workers")
@ComponentScan(basePackages = "com.freelancingpeter.data")
@EntityScan(basePackages = "com.freelancingpeter.data")
@EnableJpaRepositories("com.freelancingpeter.data")
@EnableWebSecurity
@Configuration
public class Application extends SpringBootServletInitializer implements AppShellConfigurator, WebMvcConfigurer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1831875954710899959L;

	ScheduledExecutorService executor = null;

	public static void main(String[] args) {

		SpringApplication app = new SpringApplication(Application.class);

		app.setBanner(new Banner() {

			@Override
			public void printBanner(Environment environment, Class<?> sourceClass, PrintStream out) {
				out.println("###########################################");
				out.println("#############FreelancingPeter#############");
				out.println("###Created by (codeberg.com/)spectral369###");
				out.println("###########################################");

			}
		});

		SpringApplication.run(Application.class, args);
	}

	@Override
	public void configurePage(AppShellSettings settings) {

		settings.addMetaTag("description", "Freelancingpeter - Personal Web Site");
		settings.addLink("start", "/");
		settings.addMetaTag("keywords", "freelancing, freelance, peter, freelancingpeter, peterfreelance, spectral369");
		settings.addMetaTag("og:title", "Freelancing Peter");
		settings.addMetaTag("og:type", "Development");
		settings.addMetaTag("og:url", "freelancingpeter.eu");
		settings.addMetaTag("og:image", "https://static.thenounproject.com/png/111266-200.png");
		settings.addLink("canonical", "https://freelancingpeter.eu/");
		settings.addMetaTag("Access-Control-Allow-Origin", "https://counter.dev/");
		settings.addMetaTag("Access-Control-Allow-Origin", "https://cdn.counter.dev");
		settings.addMetaTag("Access-Control-Allow-Origin", "https://freelancingpeter.eu/");
		settings.addMetaTag("Access-Control-Allow-Credentials", "true");
		settings.addMetaTag("Access-Control-Allow-Headers", "Content-Type");
		settings.addMetaTag("Access-Control-Allow-Methods", "GET, DELETE, HEAD, OPTIONS");
		settings.addMetaTag("google-site-verification", "tWuh1W7Qk3k_bJE20N7myp_DvjfcxroYbbNvX-JAKpw");
		// UI.getCurrent().getPushConfiguration().setPushUrl("https://freelancingpeter.eu");
	}

	@Bean
	TomcatContextCustomizer sessionCookieConfigForCors() {
		return context -> {
			final Rfc6265CookieProcessor cookieProcessor = new Rfc6265CookieProcessor() {
				@Override
				public String generateHeader(Cookie cookie, HttpServletRequest request) {

					// Needs to be secure
					if (cookie.getName().contains("JSESSIONID")) {
						cookie.setSecure(true);
						cookie.setPath("/");
						cookie.setDomain("freelancingpeter.eu");// freelancingpeter.eu
						cookie.setAttribute("SameSite", SameSiteCookies.STRICT.getValue());
						cookie.setHttpOnly(true);
						// cookie.setAttribute("Partitioned", "true");
					}
					if (cookie.getName().contains("csrfToken")) {
						cookie.setSecure(true);
						cookie.setPath("/");
						cookie.setDomain("freelancingpeter.eu");// freelancingpeter.eu
						cookie.setAttribute("SameSite", SameSiteCookies.STRICT.getValue());
						cookie.setHttpOnly(true);
						// cookie.setAttribute("Partitioned", "true");
					}
					return super.generateHeader(cookie, request);
				}
			};
			context.setCookieProcessor(cookieProcessor);
		};
	}

	@Bean
	ServletContextInitializer servletContextInitializer() {
		return new ServletContextInitializer() {
			@Override
			public void onStartup(ServletContext servletContext) throws ServletException {
				servletContext.setSessionTrackingModes(Collections.singleton(SessionTrackingMode.COOKIE));

				SessionCookieConfig sessionCookieConfig = servletContext.getSessionCookieConfig();
				sessionCookieConfig.setHttpOnly(true);
				sessionCookieConfig.setSecure(true);
				sessionCookieConfig.setPath("/");
				sessionCookieConfig.setDomain("freelancingpeter.eu");
				sessionCookieConfig.setAttribute("SameSite", SameSiteCookies.STRICT.getValue());

			}
		};
	}

	@Bean
	CookieSameSiteSupplier applicationCookieSameSiteSupplier() {
		return CookieSameSiteSupplier.ofStrict();
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/").allowedOrigins("https://cdn.counter.dev", "https://freelancingpeter.eu")
				.allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS").allowedHeaders("*").allowCredentials(true)
				.maxAge(3600);
	}

	@Bean
	SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		http.csrf((csrf) -> {
			csrf.csrfTokenRepository(new CustomCsrfTokenRepository());
			csrf.disable();

		})

				.headers(httpSecurityHeadersConfigurer -> httpSecurityHeadersConfigurer
						.xssProtection(Customizer.withDefaults())
						.httpStrictTransportSecurity(
								hsts -> hsts.includeSubDomains(true).preload(true).maxAgeInSeconds(31536000))

						.contentSecurityPolicy(csp -> csp.policyDirectives(
								"script-src 'self' 'unsafe-eval' 'sha384-JAsaFb2tAI/3zg8Pm8a6aOGyv/QFPKNp4ZNZUU9TJypp+E+0HQvN0UkL5bVyZNpX'; script-src-attr 'self' https://cdn.freelancingpeter.eu; script-src-elem https://cdn.counter.dev https://freelancingpeter.eu https://fonts.googleapis.com 'unsafe-inline'; "
										+ "object-src 'none'; style-src 'self' https://cdn.freelancingpeter.eu; style-src-elem https://fonts.googleapis.com https://freelancingpeter.eu 'unsafe-inline';"
										+ "style-src-attr 'unsafe-inline'; frame-ancestors 'self'; default-src 'none'; base-uri https://freelancingpeter.eu;"
										+ "form-action 'self';font-src 'self' https://cdn.freelancingpeter.eu  https://fonts.gstatic.com data:;"
										+ " connect-src 'self' https://*.freelancingpeter.eu https://freelancingpeter.eu https://*.counter.dev wss://freelancingpeter.eu;" // ws://localhost:35729
										+ "img-src 'self' https://cdn.freelancingpeter.eu https://steamuserimages-a.akamaihd.net "
										+ "https://avatars.steamstatic.com  https://media.steampowered.com https://opgg-static.akamaized.net"
										+ " data: w3.org/svg/2000; require-trusted-types-for 'script';manifest-src https://freelancingpeter.eu; frame-src 'self' blob: freelancingpeter.eu; upgrade-insecure-requests"))); // blob:
		// localhost:8080

		return http.build();
	}
}

class CustomCsrfTokenRepository implements CsrfTokenRepository {
	static final String DEFAULT_CSRF_COOKIE_NAME = "XSRF-TOKEN";

	static final String DEFAULT_CSRF_PARAMETER_NAME = "_csrf";

	static final String DEFAULT_CSRF_HEADER_NAME = "X-XSRF-TOKEN";

	private static final String CSRF_TOKEN_REMOVED_ATTRIBUTE_NAME = CookieCsrfTokenRepository.class.getName()
			.concat(".REMOVED");

	private String parameterName = DEFAULT_CSRF_PARAMETER_NAME;

	private String headerName = DEFAULT_CSRF_HEADER_NAME;

	private String cookieName = DEFAULT_CSRF_COOKIE_NAME;

	private String cookiePath;

	private int cookieMaxAge = -1;

	private Consumer<ResponseCookie.ResponseCookieBuilder> cookieCustomizer = (builder) -> {
	};

	/**
	 * Add a {@link Consumer} for a {@code ResponseCookieBuilder} that will be
	 * invoked for each cookie being built, just before the call to {@code build()}.
	 * 
	 * @param cookieCustomizer consumer for a cookie builder
	 * @since 6.1
	 */
	public void setCookieCustomizer(Consumer<ResponseCookie.ResponseCookieBuilder> cookieCustomizer) {

		this.cookieCustomizer = cookieCustomizer;
	}

	@Override
	public CsrfToken generateToken(HttpServletRequest request) {
		return new DefaultCsrfToken(this.headerName, this.parameterName, createNewToken());
	}

	@Override
	public void saveToken(CsrfToken token, HttpServletRequest request, HttpServletResponse response) {
		String tokenValue = (token != null) ? token.getToken() : "";

		ResponseCookie.ResponseCookieBuilder cookieBuilder = ResponseCookie.from(this.cookieName, tokenValue)
				.secure(true).path("/").maxAge((token != null) ? this.cookieMaxAge : 0).httpOnly(true)
				.domain("freelancingpeter.eu").sameSite("strict");

		this.cookieCustomizer.accept(cookieBuilder);

		ResponseCookie responseCookie = cookieBuilder.build();
		if (!StringUtils.hasLength(responseCookie.getSameSite())) {
			Cookie cookie = mapToCookie(responseCookie);
			response.addCookie(cookie);
		} else if (request.getServletContext().getMajorVersion() > 5) {
			Cookie cookie = mapToCookie(responseCookie);
			response.addCookie(cookie);
		} else {
			response.addHeader(HttpHeaders.SET_COOKIE, responseCookie.toString());
		}

		// Set request attribute to signal that response has blank cookie value,
		// which allows loadToken to return null when token has been removed
		if (!StringUtils.hasLength(tokenValue)) {
			request.setAttribute(CSRF_TOKEN_REMOVED_ATTRIBUTE_NAME, Boolean.TRUE);
		} else {
			request.removeAttribute(CSRF_TOKEN_REMOVED_ATTRIBUTE_NAME);
		}
	}

	@Override
	public CsrfToken loadToken(HttpServletRequest request) {
		// Return null when token has been removed during the current request
		// which allows loadDeferredToken to re-generate the token
		if (Boolean.TRUE.equals(request.getAttribute(CSRF_TOKEN_REMOVED_ATTRIBUTE_NAME))) {
			return null;
		}
		Cookie cookie = WebUtils.getCookie(request, this.cookieName);
		if (cookie == null) {
			return null;
		}
		String token = cookie.getValue();
		if (!StringUtils.hasLength(token)) {
			return null;
		}
		return new DefaultCsrfToken(this.headerName, this.parameterName, token);
	}

	/**
	 * Sets the name of the HTTP request parameter that should be used to provide a
	 * token.
	 * 
	 * @param parameterName the name of the HTTP request parameter that should be
	 *                      used to provide a token
	 */
	public void setParameterName(String parameterName) {

		this.parameterName = parameterName;
	}

	/**
	 * Sets the name of the HTTP header that should be used to provide the token.
	 * 
	 * @param headerName the name of the HTTP header that should be used to provide
	 *                   the token
	 */
	public void setHeaderName(String headerName) {

		this.headerName = headerName;
	}

	/**
	 * Sets the name of the cookie that the expected CSRF token is saved to and read
	 * from.
	 * 
	 * @param cookieName the name of the cookie that the expected CSRF token is
	 *                   saved to and read from
	 */
	public void setCookieName(String cookieName) {

		this.cookieName = cookieName;
	}

	private String createNewToken() {
		return UUID.randomUUID().toString();
	}

	private Cookie mapToCookie(ResponseCookie responseCookie) {
		Cookie cookie = new Cookie(responseCookie.getName(), responseCookie.getValue());
		cookie.setSecure(responseCookie.isSecure());
		cookie.setPath(responseCookie.getPath());
		cookie.setMaxAge((int) responseCookie.getMaxAge().getSeconds());
		cookie.setHttpOnly(responseCookie.isHttpOnly());
		if (StringUtils.hasLength(responseCookie.getDomain())) {
			cookie.setDomain(responseCookie.getDomain());
		}
		if (StringUtils.hasText(responseCookie.getSameSite())) {
			cookie.setAttribute("SameSite", responseCookie.getSameSite());
		}
		return cookie;
	}

	/**
	 * Set the path that the Cookie will be created with. This will override the
	 * default functionality which uses the request context as the path.
	 * 
	 * @param path the path to use
	 */
	public void setCookiePath(String path) {
		this.cookiePath = path;
	}

	/**
	 * Get the path that the CSRF cookie will be set to.
	 * 
	 * @return the path to be used.
	 */
	public String getCookiePath() {
		return this.cookiePath;
	}

}
